<?php

namespace Yadda\Enso\Newsletter\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Facade for Enso Copy
 */
class EnsoNewsletter extends Facade
{
    /**
     * Get the facade accessor
     *
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return 'ensonewsletter';
    }
}
