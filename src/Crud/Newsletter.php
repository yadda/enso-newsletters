<?php

namespace Yadda\Enso\Newsletter\Crud;

use Illuminate\Support\Facades\App;
use Illuminate\Validation\Rule;
use Yadda\Enso\Crud\Config;
use Yadda\Enso\Crud\Forms\Fields\SelectField;
use Yadda\Enso\Crud\Forms\Fields\TextField;
use Yadda\Enso\Crud\Forms\Form;
use Yadda\Enso\Crud\Forms\Section;
use Yadda\Enso\Crud\Tables\Text;
use Yadda\Enso\Newsletter\Contracts\NewsletterModelContract;
use Yadda\Enso\Newsletter\Facades\EnsoNewsletter;
use Yadda\Enso\Utilities\Helpers;

class Newsletter extends Config
{
    public function configure()
    {
        $model_class = Helpers::getConcreteClass(NewsletterModelContract::class);

        $this->model($model_class)
            ->route('admin.newsletters')
            ->views('newletters')
            ->name('Newletter')
            ->paginate(25)
            ->columns([
                Text::make('type'),
                Text::make('email'),
                Text::make('form_data')
                    ->setLabel('Notable Data')
                    ->setFormatter(function ($value, $model) {
                        $form_data = $model->getNewsletterHandler()->getImportantFormData($model);

                        return implode(", ", array_map(function ($key, $value) {
                            return implode(': ', [$key, "\"" . $value . "\""]);
                        }, array_keys($form_data), $form_data));
                    }),
                Text::make('submitted_at'),
            ])
            ->addBulkActions([
                'download' => [
                    'route' => route('admin.newsletters.request-csv'),
                    'method' => 'GET',
                    'label' => 'Download CSV',
                ],
            ])
            ->filters([
                'form_type' => \Yadda\Enso\Newsletter\Crud\Filters\NewsletterSelectFilter::make(),
                'search' => \Yadda\Enso\Newsletter\Crud\Filters\NewsletterFilter::make(),
            ])
            ->rules([
                'main.type.id' => ['required', Rule::in(EnsoNewsletter::types())],
                'main.email' => ['required', 'email'],
            ]);
    }

    public function create(Form $form)
    {
        $form->addSections([
            Section::make('main')
                ->addFields([
                    SelectField::make('type')
                        ->setOptions(EnsoNewsletter::options())
                        ->addFieldsetClass('is-half'),
                    TextField::make('email')
                        ->addFieldsetClass('is-half'),
                ]),
        ]);

        return $form;
    }
}
