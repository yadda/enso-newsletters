<?php

namespace Yadda\Enso\Newsletter\Crud\Filters;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Config;
use Yadda\Enso\Crud\Filters\BaseFilters\SelectFilter;
use Yadda\Enso\Newsletter\Facades\EnsoNewsletter;

class NewsletterSelectFilter extends SelectFilter
{
    /**
     * Columns to search in
     *
     * @var array
     */
    protected $columns = [
        'type'
    ];

    /**
     * Label to apply to the filter
     *
     * @var string
     */
    protected $label = 'Type';

    /**
     * Props to apply to the filter
     *
     * @var array
     */
    protected $props = [
        'placeholder' => 'Search...',
        'help-text' => 'Newsletter sign-ups of the selected type',
    ];

    /**
     * Settings to apply to the filter
     *
     * @var array
     */
    protected $settings = [
        'allow_empty' => false,
        'show_labels' => false,
    ];

    public function __construct()
    {
        $this->setSettings([
            'label' => Config::get('enso.settings.select_label_by'),
            'track_by' => Config::get('enso.settings.select_track_by'),
        ], true);

        $this->options(
            ['all' => 'All Types']
            + EnsoNewsletter::options()
        );

        $this->default([
            Config::get('enso.settings.select_label_by') => 'All Types',
            Config::get('enso.settings.select_track_by') => 'all',
        ]);
    }

    /**
     * Outputs this instance into a data array for the Crud. If there is less
     * than 2 options, do not show this filter.
     *
     * @return array
     */
    public function toArray(): array
    {
        if (count(EnsoNewsletter::options()) < 2) {
            return [];
        }

        return parent::toArray();
    }

    /**
     * Apply value as a relationship modifier
     *
     * @param Builder $query
     * @param mixed   $value
     *
     * @return void
     */
    protected function applyAsRelationship(Builder $query, $value): void
    {
        switch ($value) {
            case 'all':
                break;
            default:
                $query->whereHas($this->relationship_name, function ($query) use ($value) {
                    $this->applyQueryModifications($query, $value);
                });
        }
    }

    /**
     * Apply columns to the given query as a self-contained where statement
     *
     * @param Builder $query
     * @param mixed   $value
     *
     * @return void
     */
    protected function applyQueryModifications(Builder $query, $value): void
    {
        if (in_array($value, ['all'])) {
            // Do not modify query to show all
        } else {
            $query->where(function ($query) use ($value) {
                foreach ($this->columns as $column) {
                    $query->orWhere($column, $value);
                }
            });
        }
    }
}
