<?php

namespace Yadda\Enso\Newsletter\Controllers\Admin;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use TorMorten\Eventy\Facades\Eventy;
use Yadda\Enso\Crud\Config;
use Yadda\Enso\Crud\Controller;
use Yadda\Enso\Crud\Forms\Fields\DividerField;
use Yadda\Enso\Crud\Forms\Fields\StaticTextField;
use Yadda\Enso\Newsletter\Contracts\NewsletterModelContract;

class NewsletterController extends Controller
{
    /**
     * Adds a Url display to the form.
     *
     * @param Config $config
     * @param Request $request
     *
     * @return void
     */
    public static function addDataFields(Config $config, Request $request)
    {
        $form = $config->getEditForm();
        $newsletter = $form->getModelInstance();

        $newsletter_data = $newsletter->getNewsletterHandler()->getDisplayableFormData($newsletter);

        if ($newsletter && $newsletter_data) {
            $form->getSection('main')->appendField(
                DividerField::make('response_data_divider')
                    ->setTitle('Response Data')
            );

            foreach ($newsletter_data as $index => $value) {
                $form->getSection('main')->appendField(
                    StaticTextField::make('response_data_' . $index)
                        ->setContent(implode(': ', [$index, $value]))
                );
            }
        }
    }

    /**
     * Configures the hooks and filters for this controller
     *
     * @return void
     */
    protected function configureHooks()
    {
        if (!defined('ENSO_NEWSLETTER_CONTROLLER_CONFIGURE_HOOKS')) {

            Eventy::addAction('crud.edit', [static::class, 'addDataFields'], 20, 2);

            define('ENSO_NEWSLETTER_CONTROLLER_CONFIGURE_HOOKS', true);
        }
    }

    /**
     * Name of this crud type
     *
     * This will be used to find config/model/controller
     *
     * @var string
     */
    protected $crud_name = 'ensonewsletter';

    /**
     * Streams a CSV file response for the Downloads matching the given filters
     *
     * @param Request $request
     *
     * @return StreamedResponse
     */
    public function downloadCsv(Request $request): StreamedResponse
    {
        $filters = json_decode($request->input('filters', []), true);

        $query = $this->makeDownloadQuery($filters);

        /**
         * Determine form type if available.
         */
        $form_type = Arr::get($filters, 'form_type.id', null);

        $callback = function () use ($query, $form_type) {
            $file = fopen('php://output', 'w');

            if ($file === false) {
                return 'Unable to create File at this time';
            }

            fputcsv(
                $file,
                $this->getNewsletterCsvHeaders($form_type, $query->first()),
            );

            $query->chunk(50, function ($newsletters) use ($file, $form_type) {
                foreach ($newsletters as $newsletter) {
                    fputcsv(
                        $file,
                        $this->convertModelToCsvForDownload($newsletter, !empty($form_type))
                    );
                }
            });

            fclose($file);
        };

        $filename = $this->getCsvFilename($filters);

        $headers = $this->getCsvDownloadStreamHeaders($filename);

        return Response::stream($callback, 200, $headers);
    }

    /**
     * Request the URL at which to retrieve a CSV download
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function requestCsv(Request $request): JsonResponse
    {
        $values = $request->get('values', []);
        $filters = $request->get('filters', []);

        if (!empty($values)) {
            return Response::json([
                'status' => 'error',
                'message' => 'To keep CSV files simple, downloading form responses by individual selection is disabled.',
            ]);
        }

        if (!$this->makeDownloadQuery($filters)->count()) {
            return Response::json([
                'status' => 'error',
                'message' => 'The selected filters result in no matches',
            ]);
        }

        return Response::json([
            'status' => 'success',
            'data' => [
                'file_url' => route(
                    'admin.newsletters.download-csv',
                    [
                        'filters' => json_encode($request->input('filters')),
                    ]
                ),
            ],
        ]);
    }

    /**
     * Applies Crud filters to a query for downloading a CSV file. By default,
     * this applies filters in the same manner as the Index route.
     *
     * @param Builder $query
     * @param array $filters
     *
     * @return Builder
     */
    protected function applyDownloadFilters(Builder $query, array $filters): Builder
    {
        return $this->applyFilters($query, $filters);
    }

    /**
     * Converts a model instance into array of data to fill a line in a csv
     * file. This takes into account type-specific data if a single form type is
     * being used.
     *
     * @param NewsletterModelContract $newsletter
     * @param bool                    $form_type
     *
     * @return array
     */
    protected function convertModelToCsvForDownload(NewsletterModelContract $newsletter, bool $using_for_type = false): array
    {
        return array_merge(
            [
                $newsletter->type,
                $newsletter->email,
                $newsletter->created_at->format('Y-m-d H:i:s'),
            ],
            $using_for_type
                ? array_values($newsletter->getNewsletterHandler()->getDisplayableFormData($newsletter))
                : []
        );
    }

    /**
     * Headers for the csv download stream
     *
     * @param string $filename
     *
     * @return array
     */
    protected function getCsvDownloadStreamHeaders(string $filename): array
    {
        return [
            'Content-type' => 'text/csv',
            'Content-Disposition' => 'attachment; filename=' . $filename,
            'Pragma' => 'no-cache',
            'Cache-Control' => 'must-revalidate, post-check=0, pre-check=0',
            'Expires' => '0'
        ];
    }

    /**
     * Filename to offer when streaming a CSV file download
     *
     * @param array $filters
     *
     * @return string
     */
    protected function getCsvFilename(array $filters): string
    {
        return 'newsletter-signups.csv';
    }

    /**
     * Array of header labels for the CSV columns. This takes into account other
     * form data if a single form_type is being downloaded.
     *
     * @param string $form_type
     *
     * @return array
     */
    protected function getNewsletterCsvHeaders(string $form_type = null, Model $model = null): array
    {
        return array_merge(
            [
                'Type',
                'Email',
                'Sign up date'
            ],
            ($form_type && $model)
                ? array_keys($model->getNewsletterHandler()->getDisplayableFormData($model))
                : []
        );
    }

    /**
     * Generates a query for making a CSV download
     *
     * @param array $filters
     *
     * @return Builder
     */
    protected function makeDownloadQuery(array $filters): Builder
    {
        return $this->applyDownloadFilters(
            App::make(NewsletterModelContract::class)::query(),
            $filters
        );
    }
}
