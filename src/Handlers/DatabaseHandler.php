<?php

namespace Yadda\Enso\Newsletter\Handlers;

use Yadda\Enso\Newsletter\Contracts\NewsletterHandlerContract;
use Yadda\Enso\Newsletter\Traits\HasGenericFormData;
use Yadda\Enso\Newsletter\Traits\WriteNewsletterToDatabase;

/**
 * Basic Database handler. This is intended as a basic example, but if
 * this meets your needs you may use it directly.
 */
class DatabaseHandler implements NewsletterHandlerContract
{
    use WriteNewsletterToDatabase, HasGenericFormData;

    /**
     * Handle the request data
     *
     * @param array $request_data
     *
     * @return mixed
     */
    public function handle(array $request_data)
    {
        $this->writeToDatabase($request_data);
    }

    /**
     * Validation messages
     *
     * @return array
     */
    public function messages(): array
    {
        return [];
    }

    /**
     * Validation rules
     *
     * @return array
     */
    public function rules(): array
    {
        return [];
    }
}
