<?php

namespace Yadda\Enso\Newsletter\Handlers;

use Illuminate\Support\Arr;
use Yadda\Enso\Newsletter\Contracts\NewsletterHandlerContract;
use Yadda\Enso\Newsletter\Traits\HasGenericFormData;
use Yadda\Enso\Newsletter\Traits\SendNewsletterToMailchimp;

/**
 * Basic Mailchimp handler. This is intended as a basic example, but if
 * this meets your needs you may use it directly.
 */
class MailchimpHandler implements NewsletterHandlerContract
{
    use SendNewsletterToMailchimp, HasGenericFormData;

    /**
     * Handle the request data
     *
     * @param array $request_data
     *
     * @return mixed
     */
    public function handle(array $request_data)
    {
        $this->sendToMailchimp($request_data);

        return $this->mailchimpFailureResponse();
    }

    /**
     * Compute merge data from the request data.
     *
     * @param array $request_data
     *
     * @return array
     */
    protected function mailchimpMergeData(array $request_data): array
    {
        return [
            'FNAME' => Arr::get($request_data, 'first_name'),
            'LNAME' => Arr::get($request_data, 'last_name'),
        ];
    }

    /**
     * Validation messages
     *
     * @return array
     */
    public function messages(): array
    {
        return [];
    }

    /**
     * Validation rules
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'first_name' => ['required', 'string'],
            'last_name' => ['nullable', 'string'],
        ];
    }
}
