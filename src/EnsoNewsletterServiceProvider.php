<?php

namespace Yadda\Enso\Newsletter;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\ServiceProvider;
use Yadda\Enso\Facades\EnsoMenu;
use Yadda\Enso\Newsletter\Contracts\NewsletterAdminEmailContract;
use Yadda\Enso\Newsletter\Contracts\NewsletterControllerContract;
use Yadda\Enso\Newsletter\Contracts\NewsletterModelContract;
use Yadda\Enso\Newsletter\Controllers\NewsletterController;
use Yadda\Enso\Newsletter\EnsoNewsletter;
use Yadda\Enso\Newsletter\Facades\EnsoNewsletter as EnsoNewsletterFacade;
use Yadda\Enso\Newsletter\Mail\NewsletterAdminEmail;
use Yadda\Enso\Newsletter\Models\Newsletter;

class EnsoNewsletterServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        // Package config details
        $this->mergeConfigFrom(
            __DIR__ . '/installable/config/enso/newsletter.php',
            'enso.newsletter'
        );

        // Crud config details
        $this->mergeConfigFrom(
            __DIR__ . '/installable/config/enso/crud/ensonewsletter.php',
            'enso.crud.ensonewsletter'
        );

        $this->loadViewsFrom(
            __DIR__ . '/installable/resources/views/crud',
            'enso-crud'
        );

        $this->loadViewsFrom(
            __DIR__ . '/resources/views/mail',
            'enso-newsletters'
        );

        $this->publishes([
            __DIR__ . '/installable/config/enso/crud/ensonewsletter.php' => config_path('enso/crud/ensonewsletter.php'),
        ], 'enso-config');

        $this->publishes([
            __DIR__ . '/installable/config/enso/newsletter.php' => config_path('enso/newsletter.php'),
        ], 'enso-newsletter');

        // Migration is optional if not writing to database
        $this->publishes([
            __DIR__ . '/Database/Migrations' => database_path('migrations'),
        ], 'enso-migration');

        if (EnsoNewsletterFacade::crudEnabled()) {
            EnsoMenu::addItem(Config::get('enso.crud.ensonewsletter.menuitem'));
        }
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('ensonewsletter', function () {
            return new EnsoNewsletter;
        });

        $this->app->bind(NewsletterModelContract::class, Newsletter::class);
        $this->app->bind(NewsletterControllerContract::class, NewsletterController::class);
        $this->app->bind(NewsletterAdminEmailContract::class, NewsletterAdminEmail::class);
    }
}
