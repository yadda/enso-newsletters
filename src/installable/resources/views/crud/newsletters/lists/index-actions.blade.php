<div class="is-pulled-right">
  @if ($crud->isNested())
    <a href="{{ route($crud->getRoute() . '.tree') }}" class="button">Rearrange</a>
  @endif
  <a href="{{ route($crud->getRoute() . '.create') }}" class="button is-primary">Create</a>
</div>
