@component('mail::message')
# Contact Form Submission

**Email**: [{{ $email }}](mailto:{{ $email }})

@if (!empty($data))
@foreach ($data as $key => $value)
**{{ ucwords($key) }}:** {{ $value }}

@endforeach
@endif
@endcomponent
