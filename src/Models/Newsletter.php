<?php

namespace Yadda\Enso\Newsletter\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;
use Yadda\Enso\Newsletter\Contracts\NewsletterHandlerContract;
use Yadda\Enso\Newsletter\Contracts\NewsletterModelContract;

class Newsletter extends Model implements NewsletterModelContract
{
    /**
     * The model's attributes.
     *
     * @var array
     */
    protected $attributes = [
        'data' => '[]',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'data' => 'array',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'data',
        'email',
        'type',
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'enso_newsletters';

    /**
     * Attribute accessor for created at as a human readable string
     *
     * @return string
     */
    public function getSubmittedAtAttribute(): string
    {
        return $this->created_at
            ? $this->created_at->format('H:i:s, jS M Y')
            : 'Unknown';
    }

    public function getNewsletterHandler(): NewsletterHandlerContract
    {
        $newsletter_handler_class = Config::get('enso.newsletter.newsletters.' . $this->type . '.handler');

        return app()->make($newsletter_handler_class);
    }
}
