<?php

namespace Yadda\Enso\Newsletter;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Route;
use Yadda\Enso\Facades\EnsoCrud;
use Yadda\Enso\Newsletter\Contracts\NewsletterControllerContract;
use Yadda\Enso\Utilities\Helpers;

class EnsoNewsletter
{
    /**
     * Whether to provide a Crud interface for newsletters
     *
     * @return boolean
     */
    public function crudEnabled(): bool
    {
        return Config::get('enso.newsletter.enable_admin', false);
    }

    /**
     * Adds Crud routes for the newsletter submissions
     *
     * @param string $path
     * @param string $crud_name
     * @param string $name
     *
     * @return void
     */
    public function crudRoutes(
        string $path = 'admin/newsletters',
        string $crud_name = 'ensonewsletter',
        string $name = 'admin.newsletters'
    ) {
        $controller_class = EnsoCrud::controllerClass($crud_name, true);

        Route::get(
            $path . '/request-csv',
            $controller_class . '@requestCsv'
        )->name($name . '.request-csv');
        Route::get(
            $path . '/download-csv',
            $controller_class . '@downloadCsv'
        )->name($name . '.download-csv');

        EnsoCrud::crudRoutes($path, $crud_name, $name);
    }

    /**
     * An Options array for populating select fields.
     *
     * @return array
     */
    public function options(): array
    {
        return array_combine(
            $this->types(),
            array_map(function ($type) {
                return ucfirst(str_replace('-', ' ', $type));
            }, $this->types())
        );
    }

    /**
     * Add frontend routes for newsletters
     */
    public function routes(string $path = 'newsletters', string $name = 'newsletters'): void
    {
        Route::post(
            $path,
            '\\' . Helpers::getConcreteClass(NewsletterControllerContract::class) . '@store'
        )->name($name . '.store');
    }

    /**
     * Array of names of the config-defined types of newsletter
     *
     * @return array
     */
    public function types(): array
    {
        return array_keys(Config::get('enso.newsletter.newsletters', []));
    }
}
