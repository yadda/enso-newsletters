<?php

namespace Yadda\Enso\Newsletter\Traits;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

trait HasGenericFormData
{
    /**
     * Gets all displayable newsletter data. This should be presented as key-value pairs.
     *
     * @param Model $newsletter
     *
     * @return array
     */
    public function getDisplayableFormData(Model $newsletter): array
    {
        return (new Collection($newsletter->data))->map(function ($data) {
            if (is_array($data)) {
                return implode(', ', $data);
            } else {
                return $data;
            }
        })->toArray();
    }

    /**
     * Gets the important Form data for displaying on index pages
     *
     * @param Model $newsletter
     *
     * @return array
     */
    public static function getImportantFormData(Model $newsletter): array
    {
        return [];
    }
}

