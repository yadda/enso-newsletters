<?php

namespace Yadda\Enso\Newsletter\Traits;

use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Spatie\Newsletter\NewsletterFacade;
use TypeError;
use Yadda\Enso\Newsletter\Exceptions\NewsletterException;

/**
 * Sends request data to Mailchimp as a new signup. This expects that data provided
 * has already been validated.
 *
 * To use this class, you should ensure that you newsletter config array has a
 * `list_name` property that aligns with the name of a list set up in the spatie
 * configuration process.
 */
trait SendNewsletterToMailchimp
{
    /**
     * Config name of the mailchimp list to send this signup to
     *
     * @param string $type
     *
     * @return string|null
     */
    protected function mailchimpListName(string $type): ?string
    {
        return Config::get('enso.newsletter.newsletters.' . $type . '.list_name', null);
    }

    /**
     * Pluck merge data from the request data.
     *
     * @param array $request_data
     *
     * @return array
     */
    protected function mailchimpMergeData(array $request_data): array
    {
        return [];
    }

    /**
     * User-friendly description of service not available.
     *
     * @return string
     */
    protected function mailchimpServiceNotAvailable(): string
    {
        return "Newsletter signups are not currently available.";
    }

    /**
     * Saves a request to the database
     *
     * @param array $request
     *
     * @return mixed
     */
    protected function sendToMailchimp(array $request_data)
    {
        $type = Arr::get($request_data, 'type');

        try {
            $list_name = $this->mailchimpListName($type);
        } catch (TypeError | Exception $e) {
            throw new NewsletterException(
                $this->mailchimpServiceNotAvailable(),
                503,
                $e
            );
        }

        NewsletterFacade::subscribeOrUpdate(
            Arr::get($request_data, 'email'),
            array_filter(
                $this->mailchimpMergeData($request_data)
            ),
            $list_name
        );
    }

    /**
     * Response to return when sending to Mailchimp fails.
     *
     * @return mixed
     */
    protected function mailchimpFailureResponse()
    {
        if (!NewsletterFacade::lastActionSucceeded()) {
            Log::error(NewsletterFacade::getLastError());

            if (request()->ajax() || request()->wantsJson()) {
                return Response::json([
                    'message' => NewsletterFacade::getLastError()
                ], 403);
            } else {
                return Redirect::back()->withInput()->with('message', NewsletterFacade::getLastError());
            }
        }
    }
}
