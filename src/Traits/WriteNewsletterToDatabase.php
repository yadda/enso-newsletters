<?php

namespace Yadda\Enso\Newsletter\Traits;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\App;
use Yadda\Enso\Newsletter\Contracts\NewsletterModelContract;

/**
 * Saves request data as a Newsletter record. This expects that data provided
 * has already been validated
 */
trait WriteNewsletterToDatabase
{
    /**
     * Formats data to persist to the database into an array
     *
     * @param array $request_data
     *
     * @return array
     */
    protected function getDatabaseData(array $request_data): array
    {
        return [
            'type' => Arr::get($request_data, 'type'),
            'email' => Arr::get($request_data, 'email'),
            'data' => Arr::except($request_data, ['type', 'email', '_token'], []),
        ];
    }

    /**
     * Saves newsletter data to the database
     *
     * @param array $request_data
     *
     * @return NewsletterModelContract
     */
    protected function writeToDatabase(array $request_data): NewsletterModelContract
    {
        return App::make(NewsletterModelContract::class)::create(
            $this->getDatabaseData($request_data)
        );
    }
}
